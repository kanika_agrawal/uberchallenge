package com.uber.challenge.uberchallenge.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.uber.challenge.uberchallenge.model.GalleryItem;
import com.uber.challenge.uberchallenge.model.SearchedImage;
import com.uber.challenge.uberchallenge.model.GetPhotosResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlickerRepository {
    private static final String TAG = FlickerRepository.class.getName();
    private static final String FLICKER_API_KEY = "3e7cc266ae2b0e0d78e279ce8e361736";

    private FlickerApi flickerApi = null;

    public FlickerRepository() {
        this.flickerApi = FlickerService.getApi();
    }

    public LiveData<List<SearchedImage>> getImages(final String text) {
        MutableLiveData<List<SearchedImage>> images = new MutableLiveData<>();

        final retrofit2.Call<GetPhotosResponse> call =
                flickerApi
                        .fetchPhotos("flickr.photos.search",
                                FLICKER_API_KEY,
                                "json",
                                1,
                                "url_s",
                                text,
                                1);

        call.enqueue(new Callback<GetPhotosResponse>() {
            @Override
            public void onResponse(Call<GetPhotosResponse> call, Response<GetPhotosResponse> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "Successfully made the call");

                    final List<GalleryItem> galleryItems =
                            response.body().getPhotoMeta().getGalleryItems();

                    final List<SearchedImage> imageList = new ArrayList<>();
                    for (final GalleryItem item : galleryItems)
                        imageList.add(new SearchedImage(item.getUrl() == null ? "" : item.getUrl()));

                    images.postValue(imageList);

                } else
                    Log.i(TAG, "onResponse: " + response.errorBody());
            }

            @Override
            public void onFailure(Call<GetPhotosResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });

        return images;
    }
}
