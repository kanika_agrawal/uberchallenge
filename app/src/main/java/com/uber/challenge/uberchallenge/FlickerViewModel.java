package com.uber.challenge.uberchallenge;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import com.uber.challenge.uberchallenge.model.SearchedImage;
import com.uber.challenge.uberchallenge.network.FlickerDataSource;
import com.uber.challenge.uberchallenge.network.FlickerDataSourceFactory;
import com.uber.challenge.uberchallenge.network.FlickerRepository;

import java.util.List;

public class FlickerViewModel extends AndroidViewModel {

    private static final String TAG = FlickerViewModel.class.getName();
    private FlickerRepository repository = null;

//    public FlickerViewModel(@NonNull Application application) {
//        super(application);
//        repository = new FlickerRepository();
//    }
//
//    public LiveData<List<SearchedImage>> getImages(final String text) {
//        return repository.getImages(text);
//    }
//


    private LiveData<PagedList<SearchedImage>> searchedImages;
    private LiveData<PageKeyedDataSource<Integer, SearchedImage>> liveDataSource;

    public FlickerViewModel(Application application) {
        super(application);
    }

    public LiveData<PagedList<SearchedImage>> getImages(final String text) {
        FlickerDataSourceFactory restaurantDataSourceFactory = new FlickerDataSourceFactory(text);
        liveDataSource = restaurantDataSourceFactory.getFlickerLiveDataSource();

        final PagedList.Config config =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(FlickerDataSource.PAGE_SIZE)
                        .build();

        searchedImages = (new LivePagedListBuilder(restaurantDataSourceFactory, config)).build();
        return searchedImages;
    }
}
