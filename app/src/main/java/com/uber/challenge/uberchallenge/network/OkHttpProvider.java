package com.uber.challenge.uberchallenge.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpProvider {
    private static OkHttpClient instance = null;

    public static OkHttpClient getOkHttpInstance() {
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (instance == null) {
            synchronized (OkHttpProvider.class) {
                if(instance == null) {
                    instance =
                            new OkHttpClient.Builder()
                                    .readTimeout(5, TimeUnit.SECONDS)
                                    .connectTimeout(5, TimeUnit.SECONDS)
                                    .addInterceptor(interceptor)
                                    .build();
                }
            }
        }

        return instance;
    }
}
