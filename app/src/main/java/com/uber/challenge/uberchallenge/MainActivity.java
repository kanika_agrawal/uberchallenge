package com.uber.challenge.uberchallenge;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.uber.challenge.uberchallenge.model.SearchedImage;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvSearchedImages;
    private ImageAdapter adapter;
    private ImagePagedListAdapter pagedListAdapter;
    private EditText etTextSearch;
    private FlickerViewModel flickerViewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final View itemScreenContainerView = findViewById(R.id.item_screen_container);
        bindViews(itemScreenContainerView);

        getSupportActionBar().setTitle(R.string.search_images);

        flickerViewModel = ViewModelProviders.of(this).get(FlickerViewModel.class);
//        RxTextView.textChanges(etTextSearch)
//                .filter(charSequence -> charSequence.length() > 3)
//                .debounce(300, TimeUnit.MILLISECONDS)
//                .subscribe(charSequence -> flickerViewModel
//                        .getImages(charSequence.toString())
//                        .observe(this, imageRows -> adapter.update(imageRows)));

        RxTextView.textChanges(etTextSearch)
                .filter(charSequence -> charSequence.length() > 3)
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribe(charSequence -> flickerViewModel
                        .getImages(charSequence.toString())
                        .observe(this, imageRows -> pagedListAdapter.submitList(imageRows)));
    }

    private void bindViews(final View parent) {
        rvSearchedImages = parent.findViewById(R.id.rvSearchedImages);
        rvSearchedImages.setLayoutManager(new GridLayoutManager(parent.getContext(), 3));
        pagedListAdapter = new ImagePagedListAdapter();
        //adapter = new ImageAdapter();
        rvSearchedImages.setAdapter(pagedListAdapter);

        etTextSearch = parent.findViewById(R.id.etTextSearch);
    }
}
