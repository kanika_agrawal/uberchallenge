package com.uber.challenge.uberchallenge.network;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PageKeyedDataSource;

import com.uber.challenge.uberchallenge.model.SearchedImage;

public class FlickerDataSourceFactory extends DataSource.Factory {
    private MutableLiveData<PageKeyedDataSource<Integer, SearchedImage>> flickerLiveDataSource =
            new MutableLiveData<>();

    private String text;

    public FlickerDataSourceFactory(final String text) {
        this.text = text;
    }

    @Override
    public DataSource create() {
        FlickerDataSource flickerDataSource = new FlickerDataSource(text);
        flickerLiveDataSource.postValue(flickerDataSource);
        return flickerDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, SearchedImage>> getFlickerLiveDataSource() {
        return flickerLiveDataSource;
    }
}
