package com.uber.challenge.uberchallenge;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.uber.challenge.uberchallenge.model.SearchedImage;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private static final String TAG = ImageAdapter.class.getName();

    private List<SearchedImage> images = null;
    private LayoutInflater inflater = null;

    public ImageAdapter() {
        images = new ArrayList<>();
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(inflater == null)
            inflater = LayoutInflater.from(parent.getContext());

        final View view = inflater.inflate(R.layout.image_row, parent, false);

        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int position) {
        final SearchedImage image = images.get(position);
        try {
            Picasso.get().load(image.getUrl()).into(imageViewHolder.imgSearchedImage);
            Picasso.get().setIndicatorsEnabled(true);
        } catch (final Exception ex) {
            Log.e(TAG, "Exception occurred due to:", ex);
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgSearchedImage;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);

            imgSearchedImage = itemView.findViewById(R.id.imgSearchedImage);
        }
    }

    public void update(List<SearchedImage> searchedImages) {
        this.images.clear();
        this.images.addAll(searchedImages);
        notifyDataSetChanged();
    }
}
