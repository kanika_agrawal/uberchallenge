package com.uber.challenge.uberchallenge.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class FlickerService {
    private static final String BASE_URL = "https://api.flickr.com/services/";

    private static Retrofit retrofit = null;
    private static FlickerApi api = null;

    private static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpProvider.getOkHttpInstance())
                    .build();
        }
        return retrofit;
    }

    public static FlickerApi getApi() {
        if (api == null) {
            synchronized (FlickerService.class) {
                if (api == null)
                    api = getRetrofit().create(FlickerApi.class);
            }
        }

        return api;
    }
}
