package com.uber.challenge.uberchallenge.network;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import com.uber.challenge.uberchallenge.model.GalleryItem;
import com.uber.challenge.uberchallenge.model.GetPhotosResponse;
import com.uber.challenge.uberchallenge.model.SearchedImage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlickerDataSource extends PageKeyedDataSource<Integer, SearchedImage> {
    private static final String TAG = FlickerDataSource.class.getName();
    private static final String FLICKER_API_KEY = "3e7cc266ae2b0e0d78e279ce8e361736";

    public static final int FIRST_PAGE = 1;
    public static final int PAGE_SIZE = 100;

    private String text;

    private FlickerApi flickerApi;

    public FlickerDataSource(final String text) {
        this.text = text;
        this.flickerApi = FlickerService.getApi();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params,
                            @NonNull LoadInitialCallback<Integer, SearchedImage> callback) {

        MutableLiveData<List<SearchedImage>> images = new MutableLiveData<>();


        final retrofit2.Call<GetPhotosResponse> call =
                flickerApi
                        .fetchPhotos("flickr.photos.search",
                                FLICKER_API_KEY,
                                "json",
                                1,
                                "url_s",
                                text,
                                FIRST_PAGE);

        call.enqueue(new Callback<GetPhotosResponse>() {
            @Override
            public void onResponse(Call<GetPhotosResponse> call, Response<GetPhotosResponse> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "Successfully made the call");

                    final List<GalleryItem> galleryItems =
                            response.body().getPhotoMeta().getGalleryItems();

                    final List<SearchedImage> imageList = new ArrayList<>();
                    for (final GalleryItem item : galleryItems)
                        imageList.add(new SearchedImage(item.getUrl() == null ? "" : item.getUrl()));

                    callback.onResult(imageList, null, FIRST_PAGE + 1);

                } else
                    Log.i(TAG, "onResponse: " + response.errorBody());
            }

            @Override
            public void onFailure(Call<GetPhotosResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params,
                           @NonNull LoadCallback<Integer, SearchedImage> callback) {

        final retrofit2.Call<GetPhotosResponse> call =
                flickerApi
                        .fetchPhotos("flickr.photos.search",
                                FLICKER_API_KEY,
                                "json",
                                1,
                                "url_s",
                                text,
                                params.key);

        call.enqueue(new Callback<GetPhotosResponse>() {
            @Override
            public void onResponse(Call<GetPhotosResponse> call, Response<GetPhotosResponse> response) {
                if (response.isSuccessful()) {
                    final Integer key = params.key > 1 ? params.key - 1 : null;
                    Log.d(TAG, "Successfully made the call");

                    final List<GalleryItem> galleryItems =
                            response.body().getPhotoMeta().getGalleryItems();

                    final List<SearchedImage> imageList = new ArrayList<>();
                    for (final GalleryItem item : galleryItems)
                        imageList.add(new SearchedImage(item.getUrl() == null ? "" : item.getUrl()));


                    callback.onResult(imageList, key);
                }
            }

            @Override
            public void onFailure(final Call<GetPhotosResponse> call, final Throwable t) {
                /* This case can be handled in various ways
                 * We can show the "No Internet Connection" message in Action Bar at the top and leave the remaining view as is.
                 * We can clean the whole view and display message "No Internet Connection" or a relevant message depending on the error
                 */
            }
        });
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params,
                          @NonNull LoadCallback<Integer, SearchedImage> callback) {

        final retrofit2.Call<GetPhotosResponse> call =
                flickerApi
                        .fetchPhotos("flickr.photos.search",
                                FLICKER_API_KEY,
                                "json",
                                1,
                                "url_s",
                                text,
                                params.key);


        call.enqueue(new Callback<GetPhotosResponse>() {
            @Override
            public void onResponse(final Call<GetPhotosResponse> call,
                                   final Response<GetPhotosResponse> response) {


                if (response.isSuccessful()) {
                    final Integer key = response.body()!= null ? params.key + 1 : null;
                    Log.d(TAG, "Successfully made the call");

                    final List<GalleryItem> galleryItems =
                            response.body().getPhotoMeta().getGalleryItems();

                    final List<SearchedImage> imageList = new ArrayList<>();
                    for (final GalleryItem item : galleryItems)
                        imageList.add(new SearchedImage(item.getUrl() == null ? "" : item.getUrl()));

                    callback.onResult(imageList, key);
                }
            }

            @Override
            public void onFailure(final Call<GetPhotosResponse> call, final Throwable t) {
                /* This case can be handled in various ways
                 * We can show the "No Internet Connection" message in Action Bar at the top and leave the remaining view as is.
                 * We can clean the whole view and display message "No Internet Connection" or a relevant message depending on the error
                */
            }
        });
    }

}
