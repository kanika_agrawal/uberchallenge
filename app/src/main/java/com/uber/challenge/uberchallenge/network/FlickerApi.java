package com.uber.challenge.uberchallenge.network;

import com.uber.challenge.uberchallenge.model.GetPhotosResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickerApi {

    @GET("rest/")
    Call<GetPhotosResponse> fetchPhotos(@Query("method") String method,
                                        @Query("api_key") String apiKey,
                                        @Query("format") String json,
                                        @Query("nojsoncallback") int noJsonCallBack,
                                        @Query("extras") String extras,
                                        @Query("text") String searchParam,
                                        @Query("page") int page);
}
