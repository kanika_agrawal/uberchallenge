package com.uber.challenge.uberchallenge;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.uber.challenge.uberchallenge.model.SearchedImage;

public class ImagePagedListAdapter extends PagedListAdapter<SearchedImage, ImagePagedListAdapter.ViewHolder> {
    private static final String TAG = ImagePagedListAdapter.class.getName();
    private LayoutInflater inflater;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgSearchedImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgSearchedImage = itemView.findViewById(R.id.imgSearchedImage);
        }
    }

    public ImagePagedListAdapter() {
        super(DIFF_CALLBACK);
    }

    private static DiffUtil.ItemCallback<SearchedImage> DIFF_CALLBACK = new DiffUtil.ItemCallback<SearchedImage>() {
        @Override
        public boolean areItemsTheSame(@NonNull SearchedImage oldItem, @NonNull SearchedImage newItem) {
            return oldItem.getUrl().equals(newItem.getUrl());
        }

        @Override
        public boolean areContentsTheSame(@NonNull SearchedImage oldItem, @NonNull SearchedImage newItem) {
            return oldItem.getUrl().equals(newItem.getUrl());
        }
    };

    @NonNull
    @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(inflater == null)
            inflater = LayoutInflater.from(parent.getContext());

        final View view = inflater.inflate(R.layout.image_row, parent, false);

        return new ViewHolder(view);
    }

    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            Picasso.get().load(getItem(position).getUrl()).into(holder.imgSearchedImage);
        } catch (final Exception ex) {
            Log.e(TAG, "Exception occurred due to:", ex);
        }
    }
}
