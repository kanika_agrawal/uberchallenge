package com.uber.challenge.uberchallenge.model;

public class SearchedImage {
    private String url;

    public SearchedImage(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
